package com.skyleanderson.PROJECT;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.skyleanderson.PROJECT.states.GamePlayState;

public class BASE_GAME extends StateBasedGame
{

	// TODO refactor this class name and give it a title
	public static final String TITLE       = "GAME TITLE";

	public static final int    GAME_WIDTH  = 800;
	public static final int    GAME_HEIGHT = 600;

	public BASE_GAME( String title )
	{
		super(TITLE);
	}

	public BASE_GAME( )
	{
		super(TITLE);
	}

	@Override
	public void initStatesList( GameContainer gc ) throws SlickException
	{
		// this.addState(new StartScreenState());
		this.addState(new GamePlayState());
		// this.addState(new PauseState());
		// this.addState(new EndGameState());
	}

	/**
	 * @param args
	 */
	public static void main( String[] args )
	{
		try
		{

			AppGameContainer container = new AppGameContainer(new BASE_GAME(TITLE));
			container.setDisplayMode(GAME_WIDTH, GAME_HEIGHT, false);
			container.start();
		} catch (SlickException e)
		{
			e.printStackTrace();
		}
	}

}
