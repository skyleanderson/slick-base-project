package com.skyleanderson.util;

import java.util.Collection;
import java.util.LinkedList;

import org.newdawn.slick.Color;
import org.newdawn.slick.tiled.TiledMap;

public class Geomap {
	
	protected LinkedList<Prop>[] regions;
	protected LinkedList<Prop> currentProps;
	protected TiledMap map;
	protected int xDivisions;
	protected int yDivisions;
	protected float regionWidth;
	protected float regionHeight;
	
	@SuppressWarnings("unchecked")
	public Geomap(TiledMap map, int xDivisions, int yDivisions)
	{
		this.map = map;
		this.xDivisions = xDivisions;
		this.yDivisions = yDivisions;
		
		regionWidth = (map.getWidth() * map.getTileWidth()) / xDivisions;
		regionHeight = (map.getHeight() * map.getTileHeight()) / yDivisions;
		
		//ugly array of generics hack.. typecast the generic array as the typed array:
		this.regions = (LinkedList<Prop>[]) new LinkedList[(xDivisions * yDivisions) ];
		
		for ( int i=0; i< xDivisions*yDivisions; i++ )
		{
			this.regions[i] = new LinkedList<Prop>();
		}
		
		currentProps = new LinkedList<Prop>();
		
	}
	
	/**
	 * Adds the given prop to the supplied region.
	 * @param regionID
	 * @param p
	 */
	public void addToRegion(int regionID, Prop p)
	{
		if (regionID < 0 ||
			regionID > xDivisions * yDivisions)
			throw new IllegalArgumentException("regionID is out of bounds. " + regionID + " :: [0, " + xDivisions * yDivisions + "]");
		
		this.regions[regionID].add(p);
	}
	
	/**
	 * Removes the given prop from the supplied region, if it is there.
	 * @param regionID
	 * @param p
	 */
	public void removeFromRegion(int regionID, Prop p)
	{
		if (regionID < 0 ||
				regionID > xDivisions * yDivisions)
				throw new IllegalArgumentException("regionID is out of bounds.");
			
			this.regions[regionID].remove(p);
	}
	
	/**
	 * Returns the ID of the region that contains the point (x, y)
	 * @param x
	 * @param y
	 * @return
	 */
	public int getRegionID(float x, float y)
	{
//		if (x < 0 || y < 0 ||
//			x > map.getWidth() * map.getTileWidth() || y > map.getHeight() * map.getTileHeight())
//			return -1;
		
		int xIndex, yIndex;
		
		xIndex = (int) (x / this.regionWidth);
		if (xIndex < 0)
			xIndex = 0;
		else if (xIndex >= xDivisions)
			xIndex = xDivisions - 1;
		yIndex = (int) (y / this.regionHeight);
		if (yIndex < 0)
			yIndex = 0;
		else if (yIndex >= yDivisions)
			yIndex = yDivisions - 1;
		
		int result = (yIndex * xDivisions) + xIndex;
		return result;
	}
	
	public LinkedList<Prop> getRegionProps(int regionID)
	{
		if (regionID < 0 ||
				regionID > xDivisions * yDivisions)
			throw new IllegalArgumentException("regionID is out of bounds.");
		
		return this.regions[regionID];
	}
	
	public LinkedList<Prop> getAreaProps(float left, float top, float right, float bottom)
	{
//		LinkedList<Prop> topLeft     = this.getRegionProps(getRegionID(left, top));
//		LinkedList<Prop> topRight    = this.getRegionProps(getRegionID(right, top));
//		LinkedList<Prop> bottomLeft  = this.getRegionProps(getRegionID(left, bottom));
//		LinkedList<Prop> bottomRight = this.getRegionProps(getRegionID(right, bottom));
//		
//		currentProps.clear();
//		currentProps.addAll(topLeft);
//		currentProps.addAll(topRight);
//		currentProps.addAll(bottomLeft);
//		currentProps.addAll(bottomRight);
//		
//		return new LinkedList<Prop>(currentProps);
		
		currentProps.clear();
		
		int startX, endX, startY, endY;
		startX = (int) (left / this.regionWidth);
		if (startX < 0)
			startX = 0;
		else if (startX >= xDivisions)
			startX = xDivisions - 1;		
		endX = (int) (right / this.regionWidth);
			if (endX < 0)
				endX = 0;
			else if (endX >= xDivisions)
				endX = xDivisions - 1;
		
		startY = (int) (top / this.regionHeight);
		if (startY < 0)
			startY = 0;
		else if (startY >= yDivisions)
			startY = yDivisions - 1;
		endY = (int) (bottom / this.regionHeight);
		if (endY < 0)
			endY = 0;
		else if (endY >= yDivisions)
			endY = yDivisions - 1;
		
		int id;
		for (int y = startY; y <= endY; y++)
		{
			for (int x = startX; x <= endX; x++)
			{
				id = (y * xDivisions) + x;
				currentProps.addAll(this.regions[id]);
			}
		}
		return new LinkedList<Prop>(currentProps);

	}	
	
	public void getAreaProps(Collection<Prop> list, float left, float top, float right, float bottom)
	{
//		LinkedList<Prop> topLeft     = this.getRegionProps(getRegionID(left, top));
//		LinkedList<Prop> topRight    = this.getRegionProps(getRegionID(right, top));
//		LinkedList<Prop> bottomLeft  = this.getRegionProps(getRegionID(left, bottom));
//		LinkedList<Prop> bottomRight = this.getRegionProps(getRegionID(right, bottom));
//		
//		list.clear();
//		list.addAll(topLeft);
//		list.addAll(topRight);
//		list.addAll(bottomLeft);
//		list.addAll(bottomRight);
		
		list.clear();
		
		int startX, endX, startY, endY;
		startX = (int) (left / this.regionWidth);
		if (startX < 0)
			startX = 0;
		else if (startX >= xDivisions)
			startX = xDivisions - 1;		
		endX = (int) (right / this.regionWidth);
			if (endX < 0)
				endX = 0;
			else if (endX >= xDivisions)
				endX = xDivisions - 1;
		
		startY = (int) (top / this.regionHeight);
		if (startY < 0)
			startY = 0;
		else if (startY >= yDivisions)
			startY = yDivisions - 1;
		endY = (int) (bottom / this.regionHeight);
		if (endY < 0)
			endY = 0;
		else if (endY >= yDivisions)
			endY = yDivisions - 1;
		
		int id;
		for (int y = startY; y <= endY; y++)
		{
			for (int x = startX; x <= endX; x++)
			{
				id = (y * xDivisions) + x;
				list.addAll(this.regions[id]);
			}
		}
		
	}
	
	/**
	 * This method populates the Geomap with props from the TiledMap. Every Tile with ID > 1 will become a prop
	 * and will be placed in the appropriate region.
	 * @param propLayerName The name of the layer to add props from
	 * @return A LinkedList<Prop> of all of the newly created Props, in case they need to be modified later.
	 */
	public LinkedList<Prop> GenerateProps(String propLayerName) {
		
		int layerID = map.getLayerIndex(propLayerName);
		if (layerID == -1)
			throw new IllegalArgumentException("Layer: " + propLayerName + " not found.");
		
		int rows = map.getHeight();
		int cols = map.getWidth();
		int width = map.getTileWidth();
		int height = map.getTileHeight();
		
		int currentRegionID;
		
		LinkedList<Prop> allProps = new LinkedList<Prop>();
		
		for (int row = 0; row < rows; row++)
		{
			for (int col = 0; col < cols; col++)
			{
				int id = map.getTileId(col, row, layerID);
				if (id > 1)
				{
					Prop p = new Prop(Color.blue, width, height);
					p.boundingBox.fill= true;
					p.setPosition(col * width, row * height);
					
					//add to all props
					allProps.add(p);
					
					//add to Geomap regions (up to 4)
					currentRegionID = getRegionID(p.left(), p.top());
					
					if (currentRegionID == getRegionID(p.right(), p.bottom()))
						//case 1, prop is located in one region
						this.addToRegion(currentRegionID, p);
					
					else if (currentRegionID == getRegionID(p.left(), p.bottom()))
					{
						//case 2, prop is in two horizontally adjacent regions
						this.addToRegion(currentRegionID, p);
						this.addToRegion(getRegionID(p.left(), p.bottom()), p);
					} else if (currentRegionID == getRegionID(p.right(), p.top()))
					{
						//case 3, prop is in two vertically adjacent regions
						this.addToRegion(currentRegionID, p);
						this.addToRegion(getRegionID(p.right(), p.top()), p);
					} else
					{
						//case 4, prop is in 4 different regions
						this.addToRegion(currentRegionID, p);
						this.addToRegion(getRegionID(p.right(), p.top()), p);
						this.addToRegion(getRegionID(p.left(), p.bottom()), p);
						this.addToRegion(getRegionID(p.right(), p.bottom()), p);
					}
				}
			}
		}
		
		return allProps;
	}
	
	/**
	 * Returns a list of all of the regions this prop lies in. This assumes every prop is sized less
	 * than 2*regionWidth by 2*regionHeight. ** Primarily used for enemies in this case.
	 * @param p
	 * @return
	 */
	public void updateAreaRegions(Prop p)
	{
		
		for (Integer I : p.mapIndexes)
		{
			this.removeFromRegion(I, p);
		}
		p.mapIndexes.clear();
		
		int topleftID = getRegionID(p.left(), p.top());
		int toprightID = getRegionID(p.right(), p.top());
		int botleftID = getRegionID(p.left(), p.bottom());
		int botrightID = getRegionID(p.right(), p.bottom());
		
		if (topleftID == botrightID)
			//case 1, prop is located in one region
			p.mapIndexes.add(topleftID);
		
		else if (topleftID == botleftID)
		{
			//case 2, prop is in two horizontally adjacent regions
			p.mapIndexes.add(topleftID);
			p.mapIndexes.add(toprightID);
		} else if (topleftID == toprightID)
		{
			//case 3, prop is in two vertically adjacent regions
			p.mapIndexes.add(topleftID);
			p.mapIndexes.add(botleftID);
		} else
		{
			//case 4, prop is in 4 different regions
			p.mapIndexes.add(topleftID);
			p.mapIndexes.add(toprightID);
			p.mapIndexes.add(botleftID);
			p.mapIndexes.add(botrightID);
		}

	}

}
