package com.skyleanderson.util;

import java.util.LinkedList;
import java.util.Random;

public class RogueLikeLevel
{
	public static final int    DEFAULT_WIDTH            = 20;
	public static final int    DEFAULT_HEIGHT           = 20;

	public static final double DEFAULT_FLOOR_PERCENTAGE = .6f;
	public static final int    DEFAULT_CA_ITERATIONS    = 2;

	public static final int    DEFAULT_CA_MIN_SAFE      = 4;
	public static final int    DEFAULT_CA_MAX_SAFE      = 5;

	public static final int    EDGE_WALL                = -1;
	public static final int    WALL                     = 0;
	public static final int    FLOOR                    = 1;
	public static final int    START_POSITION           = 1000;
	public static final int    END_POSITION             = 1001;
	public static final int    FIRST_ENEMY              = 1002;

	public RogueLikeLevel( )
	{
		this(DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}

	public RogueLikeLevel( int w, int h )
	{
		this.width = w;
		this.height = h;
		level = new int[width * height];
		swap = new int[width * height];
	}

	public int                       width;
	public int                       height;

	public int[]                     level;
	public int[]                     swap;

	public int                       caIterations         = DEFAULT_CA_ITERATIONS;
	public double                    floorPercentage      = DEFAULT_FLOOR_PERCENTAGE;

	public int                       caMinSafe            = DEFAULT_CA_MIN_SAFE;
	public int                       caMaxSafe            = DEFAULT_CA_MAX_SAFE;

	protected boolean                includeStartPosition = true;
	protected boolean                includeEndPosition   = true;
	protected LinkedList<ObjectType> enemiesToAdd         = new LinkedList<ObjectType>();
	protected int                    currentEnemyID       = FIRST_ENEMY;

	public int getWidth( )
	{
		return width;
	}

	public void setWidth( int w )
	{
		this.width = w;
	}

	public int getHeight( )
	{
		return height;
	}

	public void setHeight( int h )
	{
		this.height = h;
	}

	public int getCAMinSafe( )
	{
		return this.caMinSafe;
	}

	public void setCAMinSafe( int value )
	{
		this.caMinSafe = value;
	}

	public int getCAMaxSafe( )
	{
		return this.caMaxSafe;
	}

	public void setCAMaxSafe( int value )
	{
		this.caMaxSafe = value;
	}

	public int getCAIterations( )
	{
		return this.caIterations;
	}

	public void setCAIterations( int iterations )
	{
		this.caIterations = iterations;
	}

	public double getFloorPercentage( )
	{
		return this.floorPercentage;
	}

	public void setFloorPercentage( double percent )
	{
		this.floorPercentage = percent;
	}

	public void includeStartPosition( )
	{
		this.includeStartPosition = true;
	}

	public void includeEndPosition( )
	{
		this.includeEndPosition = true;
	}

	protected void addEnemyType( ObjectType type )
	{
		this.enemiesToAdd.add(type);
	}

	public int addObjects( int amount )
	{
		ObjectType type = new ObjectType();
		type.ID = currentEnemyID++;
		type.amount = amount;
		this.addEnemyType(type);
		return type.ID;
	}

	/**
	 * This will fill the map with 0's for walls and 1's for floor (and -1 for edge wall).
	 */
	public void generateInitialMap( double floorPercentage, long seed )
	{
		int index;
		Random r = new Random(seed);

		for (int row = 0; row < this.getHeight(); row++)
		{
			for (int col = 0; col < this.getWidth(); col++)
			{
				index = getIndex(row, col);

				if (row == 0 || col == 0 || row == this.getHeight() - 1 || col == this.getWidth() - 1)
					level[index] = EDGE_WALL;
				else if (r.nextDouble() <= floorPercentage)
					level[index] = FLOOR;
				else
					level[index] = WALL;
			}
		}

	}

	private void addObjectToRegion( int id, int minRow, int minCol, int maxRow, int maxCol )
	{
		int tileCount = (maxRow - minRow) * (maxCol - minCol);
		int position = (tileCount / 2) + (int) (Math.random() * tileCount);
		int row = minRow;
		int col = minCol;
		int i = 0;
		while (i < position)
		{
			col++;
			if (col >= maxCol)
			{
				col = minCol;
				row++;
				if (row >= maxRow) row = minRow;
			}
			if (level[getIndex(row, col)] == FLOOR) i++;
		}
		level[getIndex(row, col)] = id;
	}

	/**
	 * This will fill the map with 0's for walls and 1's for floor (and -1 for edge wall).
	 * This will use the default floor fill percentage of 40% and the current time as a random seed.
	 */
	public void generateInitialMap( )
	{
		this.generateInitialMap(floorPercentage, System.currentTimeMillis());
	}

	public void cellularAutomata( )
	{
		// standard 4-5 rule. if fewer than 4 neighbors, starve
		// if greater than 5 neighbors, merge
		cellularAutomata(caMinSafe, caMaxSafe);
	}

	public void cellularAutomata( int minSafe, int maxSafe )
	{
		int index;
		// apply the CA rule
		for (int row = 0; row < this.getHeight(); row++)
		{
			for (int col = 0; col < this.getWidth(); col++)
			{
				index = getIndex(row, col);

				if (level[index] == EDGE_WALL)
					swap[index] = EDGE_WALL;
				else
				{
					int neighbors = getNeighborCount(row, col);
					if (neighbors < minSafe)
						swap[index] = FLOOR;
					else if (neighbors > maxSafe) swap[index] = WALL;
				}
			}
		}

		// copy back to main grid
		for (int i = 0; i < (this.getHeight()) * (this.getWidth()); i++)
			level[i] = swap[i];
	}

	public int getNeighborCount( int row, int col )
	{
		int neighbors = 0;

		// upper left
		if (level[getIndex(row - 1, col - 1)] < FLOOR) neighbors++;
		// upper middle
		if (level[getIndex(row - 1, col)] < FLOOR) neighbors++;
		// upper right
		if (level[getIndex(row - 1, col + 1)] < FLOOR) neighbors++;

		// middle left
		if (level[getIndex(row, col - 1)] < FLOOR) neighbors++;
		// middle right
		if (level[getIndex(row, col + 1)] < FLOOR) neighbors++;

		// lower left
		if (level[getIndex(row + 1, col - 1)] < FLOOR) neighbors++;
		// lower middle
		if (level[getIndex(row + 1, col)] < FLOOR) neighbors++;
		// lower right
		if (level[getIndex(row + 1, col + 1)] < FLOOR) neighbors++;

		return neighbors;
	}

	public int getIndex( int row, int col )
	{
		return (row * this.getWidth() + col);
	}

	public void connectRegions( )
	{
		int regionID = FLOOR + 1;

		LinkedList<Region> regions = new LinkedList<Region>();

		// 1. assign values to different disconnected regions
		for (int row = 0; row < this.getHeight(); row++)
		{
			for (int col = 0; col < this.getWidth(); col++)
				if (level[getIndex(row, col)] == FLOOR)
				{
					this.floodFill(regionID, row, col);
					Region r = new Region();
					r.id = regionID;
					r.row = row;
					r.column = col;
					regions.add(r);
					regionID++;
				}
		}

		// 2. connect separate regions
		for (Region r : regions)
		{
			drawLineToCenter(r.id, r.row, r.column);
		}

		for (int i = 0; i < this.getHeight() * this.getWidth(); i++)
			if (level[i] > FLOOR) level[i] = FLOOR;

	}

	public void floodFill( int value, int row, int col )
	{
		floodFill(level[getIndex(row, col)], value, row, col);
	}

	private void floodFill( int oldValue, int newValue, int row, int col )
	{
		if (oldValue == newValue) return;
		if (row < 0 || col < 0 || row >= this.getHeight() || col >= this.getWidth()) return;

		if (level[getIndex(row, col)] == oldValue)
		{
			level[getIndex(row, col)] = newValue;

			this.floodFill(oldValue, newValue, row - 1, col); // left
			this.floodFill(oldValue, newValue, row + 1, col); // right
			this.floodFill(oldValue, newValue, row, col - 1); // up
			this.floodFill(oldValue, newValue, row, col + 1); // down

		} else
			return;

	}

	public void drawLineToCenter( int id, int row, int col )
	{

		int currentRow = row;
		int currentCol = col;

		int centerRow = this.getHeight() / 2;
		int centerCol = this.getWidth() / 2;

		// if this region already contains the center, then quit
		if (level[getIndex(centerRow, centerCol)] == id) return;

		int leftRightToCenter;
		int upDownToCenter;

		while (currentRow != centerRow || currentCol != centerCol)
		{
			// update current position
			leftRightToCenter = centerCol - currentCol;
			upDownToCenter = centerRow - currentRow;

			// quit drawing if we find another region (it will be connected later)
			if (level[getIndex(currentRow, currentCol)] > WALL && level[getIndex(currentRow, currentCol)] != id) return;

			level[getIndex(currentRow, currentCol)] = id;

			// find next position
			if (upDownToCenter == 0 && leftRightToCenter != 0)
			{
				// move horizontally
				if (leftRightToCenter > 0)
					currentCol++;
				else
					currentCol--;
			} else if (leftRightToCenter == 0 && upDownToCenter != 0)
			{
				// move vertically
				if (upDownToCenter > 0)
					currentRow++;
				else
					currentRow--;
			} else if (leftRightToCenter != 0 && upDownToCenter != 0)
			{
				// flip a coin
				if (Math.random() < .5)
				{
					// move horizontally
					if (leftRightToCenter > 0)
						currentCol++;
					else
						currentCol--;

				} else
				{
					// move vertically
					if (upDownToCenter > 0)
						currentRow++;
					else
						currentRow--;
				}
			}
		}

	}

	public void addObjectsToMap( )
	{
		// 1. start and end must be in different quadrants
		int startQuad = (int) (Math.random() * 3);
		int endQuad = (int) (Math.random() * 3);

		int row = 0, col = 0;
		while (endQuad == startQuad)
			endQuad = (int) (Math.random() * 3);

		int halfWidth = width / 2;
		int halfHeight = height / 2;

		// 2. start position
		if (includeStartPosition)
		{
			switch (startQuad)
			{
				case 0: // nw
					row = 0;
					col = 0;
					break;
				case 1: // ne
					row = 0;
					col = halfWidth;
					break;
				case 2: // sw
					row = halfHeight;
					col = 0;
					break;
				case 3: // se
					row = halfHeight;
					col = halfWidth;
					break;
			}
			this.addObjectToRegion(START_POSITION, row, col, row + halfHeight, col + halfWidth);
		}
		// 3. end position
		if (includeEndPosition)
		{
			switch (endQuad)
			{
				case 0: // nw
					row = 0;
					col = 0;
					break;
				case 1: // ne
					row = 0;
					col = halfWidth;
					break;
				case 2: // sw
					row = halfHeight;
					col = 0;
					break;
				case 3: // se
					row = halfHeight;
					col = halfWidth;
					break;
			}
			this.addObjectToRegion(END_POSITION, row, col, row + halfHeight, col + halfWidth);
		}

		// 4. enemies
		for (ObjectType type : enemiesToAdd)
			for (int i = 0; i < type.amount; i++)
				this.addObjectToRegion(type.ID, 0, 0, height, width);
	}

	public void generate( )
	{
		// see if the width / heigth has changed
		if (level.length != this.getWidth() * this.getHeight())
		{
			level = new int[this.getWidth() * this.getHeight()];
			swap = new int[this.getWidth() * this.getHeight()];
		}
		// clear old data
		for (int i = 0; i < this.getWidth() * this.getHeight(); i++)
		{
			level[i] = 0;
			swap[i] = 0;
		}

		// generate a random map
		generateInitialMap();

		// run the default 4-5 rule to group regrions together and become more cave-like
		for (int i = 0; i < caIterations; i++)
			cellularAutomata();

		// make sure there are no unreachable sections
		connectRegions();

		// add necessary objects to map
		addObjectsToMap();

	}

	public void clear( )
	{
		for (int i = 0; i < (this.getHeight()) * (this.getWidth()); i++)
		{
			level[i] = 0;
			swap[i] = 0;
		}

		enemiesToAdd.clear();
	}

	private class Region
	{
		public int id;
		public int row;
		public int column;
	}

	public class ObjectType
	{
		public int ID;
		public int amount;
	}

}
