package com.skyleanderson.util;

import org.newdawn.slick.tiled.TiledMap;

/**
 * This class translates from world coordinates to screen coordinates and handles the rendering of objects within
 * the viewport. It is provided the map and a Prop (which should be the player for the viewport to follow).
 * 
 * @author ska
 * 
 */
public class TiledFollowingViewPort implements ViewPort
{

	private Prop     player;
	private TiledMap map;

	private float    width;
	private float    height;

	private float    top = 0, left = 0;

	public TiledFollowingViewPort( TiledMap map, Prop prop, float width, float height )
	{
		this.map = map;
		this.player = prop;
		this.width = width;
		this.height = height;

		if (width > (map.getWidth() * map.getTileWidth()) || height > (map.getHeight() * map.getTileHeight()))
		    throw new IllegalArgumentException("Viewport is larger than the map.");

		update();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.skyleanderson.march.util.ViewPort#getLeft()
	 */
	@Override
	public float getLeft( )
	{
		return left;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.skyleanderson.march.util.ViewPort#getRight()
	 */
	@Override
	public float getRight( )
	{
		return left + width;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.skyleanderson.march.util.ViewPort#getTop()
	 */
	@Override
	public float getTop( )
	{
		return top;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.skyleanderson.march.util.ViewPort#getBottom()
	 */
	@Override
	public float getBottom( )
	{
		return top + height;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.skyleanderson.march.util.ViewPort#update()
	 */
	@Override
	public void update( )
	{
		float worldPlayerX = player.position.x;
		float worldPlayerY = player.position.y;

		float viewPlayerX = worldPlayerX - left;
		float viewPlayerY = worldPlayerY - top;

		// check x scrolling
		if (viewPlayerX > .7f * width)
		{
			left = (float) (worldPlayerX - (float) (.7f * width));
			if (this.getRight() > (map.getWidth() * map.getTileWidth()))
			{
				left = (map.getWidth() * map.getTileWidth()) - width;
			}
		} else if (viewPlayerX < .3f * width)
		{
			left = (float) (worldPlayerX - (.3f * width));
			if (left < 0)
			{
				left = 0;
			}
		}

		// check y scrolling
		if (viewPlayerY > .7f * height)
		{
			top = (float) (worldPlayerY - (float) (.7f * height));
			if (this.getBottom() > (map.getHeight() * map.getTileHeight()))
			{
				top = (map.getHeight() * map.getTileHeight()) - height;
			}
		} else if (viewPlayerY < .3f * height)
		{
			top = (float) (worldPlayerY - (.3f * height));
			if (top < 0)
			{
				top = 0;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.skyleanderson.march.util.ViewPort#getTileLeft()
	 */
	@Override
	public float getTileLeft( )
	{
		return (float) (left / map.getTileWidth());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.skyleanderson.march.util.ViewPort#getTileLeftOffset()
	 */
	@Override
	public float getTileLeftOffset( )
	{
		return left % map.getTileWidth();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.skyleanderson.march.util.ViewPort#getTileRight()
	 */
	@Override
	public float getTileRight( )
	{
		return Math.min((this.getRight() / map.getTileWidth()) + 1, map.getWidth() - 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.skyleanderson.march.util.ViewPort#getTileTop()
	 */
	@Override
	public float getTileTop( )
	{
		return (float) (top / map.getTileHeight());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.skyleanderson.march.util.ViewPort#getTileTopOffset()
	 */
	@Override
	public float getTileTopOffset( )
	{
		return top % map.getTileHeight();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.skyleanderson.march.util.ViewPort#getTileBottom()
	 */
	@Override
	public float getTileBottom( )
	{
		return Math.min((this.getBottom() / map.getTileHeight()) + 1, map.getHeight() - 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.skyleanderson.march.util.ViewPort#getTileWidth()
	 */
	@Override
	public float getTileWidth( )
	{
		return (width / map.getTileWidth()) + 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.skyleanderson.march.util.ViewPort#getTileHeight()
	 */
	@Override
	public float getTileHeight( )
	{
		return (height / map.getTileHeight()) + 1;
	}

}
